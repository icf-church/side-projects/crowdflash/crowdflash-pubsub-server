# Crowdflash

Crowdflash is an application that allows displaying colors on many smartphones and tablets at the same time, controlled via DMX interfaces.

Crowdflash consists of four main components that interact with one another in real time.

- Crowdflash Bridge
  - reads input via [ArtNet](https://gitlab.com/icf-church/side-projects/crowdflash/crowdflash-artnet-bridge) or [sACN](https://gitlab.com/icf-church/side-projects/crowdflash/crowdflash-sacn-bridge) and publishes updates to Redis
- Redis Pub/Sub Server
- [Crowdflash Websocket Server](https://gitlab.com/icf-church/side-projects/crowdflash/crowdflash-pubsub-server)
  - subscribes to Redis and pushes updates to all connected websocket clients
- [Crowdflash Client](https://gitlab.com/icf-church/side-projects/crowdflash/crowdflash-client)
  - displays the received colors on the screen

# Crowdflash Websocket Server

The Crowdflash Websocket Server allows websocket connections from many devices and publishes changes from a Redis Pub/Sub server to all connected clients.

This approach allows for a flexible and scalable setup capable of serving many hundreds, if not tens-of-thousands of devices without noticeable performance issues. The Redis server can easily handle many Crowdflash Websocket Servers as subscribers, that can each broadcast updates to hundreds of websocket clients.

## Requirements

This app requires Node.js to run. It is tested with node v16.x, but should run with lower versions just fine.


## Installation

Download the source via git or as a zip.
Install requirements by running

```bash
npm ci
```

## Usage

Running the app can be as simple as executing

```bash
node app.js
```

For a more permanent solution however it is strongly recommended to host the Crowdflash Websocket Server in a scalable
environment behind a load balancer with the capability of flexibly scaling to your requirements.

We are using the DigitalOcean App Platform to host our production environment. Feel free to take a look and try it out
yourself!

[![Deploy to DO](https://www.deploytodo.com/do-btn-blue.svg)](https://cloud.digitalocean.com/apps/new?repo=https://gitlab.com/icf-church/side-projects/crowdflash/crowdflash-pubsub-server/tree/master&refcode=2c2a7cfa1ce3)

Please note that this app currently doesn't support connecting to redis via TLS, which is required for using the
DigitalOcean Managed Redis Clusters. If you need this functionality, feel free to open an issue or just create a pull
request!

You will also (most probably) need to host the Crowdflash Bridge locally in your own network so that it can receive the
UDP data from the sACN or ArtNet source device.

## Business requests

If you would like to use Crowdflash at your event, but don't feel like setting up and managing the required
infrastructure yourself, we would be more than happy to talk to you! Feel free to reach out to us via email
at [web@icf.ch](mailto:web@icf.ch)

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License

[MIT](https://choosealicense.com/licenses/mit/) - see [LICENSE](LICENSE) file
