require('dotenv').config();
const {v4: uuidv4} = require('uuid');

const {createClient} = require('redis');
const {createServer} = require('http');
const {WebSocketServer} = require('ws');

const redisOptions = {
  socket: {
    host: process.env.REDIS_HOST,
    port: process.env.REDIS_PORT,
  },
};

// authenticate with redis
if (process.env.REDIS_PASSWORD !== undefined) {
  redisOptions.socket.password = process.env.REDIS_PASSWORD;
}

const DEFAULT_TTL = 5;
const instanceId = uuidv4();

let textItems = [
  '0',
  '1',
  '2',
  '3',
  '4',
  '5',
  '6',
  '7',
  '8',
  '9',
  '10',
  'LET\'S GO!',
  'ARE YOU READY?',
  'Cool, dass du dabei bist! Gleich geht es los...',
];

let clientData = [];

for (let i = 0; i < process.env.COLOR_CHANNELS_COUNT; i++) {
  clientData.push({color: {red: 0, green: 0, blue: 0}, textId: 255});
}

(async () => {
  // create redis client
  const redisClient = createClient(redisOptions);
  redisClient.on('error', (err) => console.error(err));
  await redisClient.connect();

  await redisClient.setEx(
    `websocket_connections:${ instanceId }`,
    DEFAULT_TTL,
    '0',
  );
  console.log('Started instance with id ' + instanceId);

  const server = createServer();
  const wss = new WebSocketServer({noServer: true});

  server.on('upgrade', (request, socket, head) => {

    wss.handleUpgrade(request, socket, head, ws => {
      wss.emit('connection', ws, request);
    });

  });

  // create & start the websocket server
  server.listen(process.env.PORT);
  console.log(`Websocket server listening on port ${ process.env.PORT }`);

  // subscribe to colorUpdate event
  const subscriber = redisClient.duplicate();
  await subscriber.connect();
  await subscriber.subscribe('colorUpdate', (message) => {
    // broadcast to all websockets when receiving a Redis PUB/SUB Event
    const payload = JSON.parse(message);
    clientData = payload.clientData;
    wss.clients.forEach((client) => {
      client.send(JSON.stringify({
        type: 'colorUpdate',
        ...clientData[client.channel],
      }));
    });
  });

  await subscriber.subscribe('textUpdate', (message) => {
    const payload = JSON.parse(message);
    textItems = payload.textItems;
    wss.clients.forEach((client) => {
      client.send(JSON.stringify({type: 'textUpdate', textItems}));
    });
  });

  wss.on('connection', (client, request) => {
    client.id = uuidv4();
    client.channel = request.url.split('/').filter(path => path.length > 0).filter(path => path !== process.env.WS_PATH);
    client.send(JSON.stringify({type: 'textUpdate', textItems}));
    client.send(JSON.stringify({
      type: 'colorUpdate',
      ...clientData[client.channel],
    }));
  });

  async function monitorCurrentConnections() {
    await redisClient.setEx(
      'websocket_connections:' + instanceId,
      DEFAULT_TTL,
      wss.clients.size.toString(),
    );

    await calculateTotalWebsocketConnections();

    setTimeout(monitorCurrentConnections, 1000);
  }

  await monitorCurrentConnections();

  async function calculateTotalWebsocketConnections() {
    let allConnections = [];
    let allConnectionsSum = 0;
    let serverKeys = [];

    await redisClient.executeIsolated(async (isolatedClient) => {
      await isolatedClient.watch('total_websocket_connections');

      serverKeys = await isolatedClient.keys('websocket_connections:*');
      allConnections = await Promise.all(
        serverKeys.map(async (key) => await isolatedClient.get(key)),
      );
      allConnectionsSum = allConnections.reduce(
        (a, b) => Number.parseInt(a) + Number.parseInt(b),
      );
    });

    await redisClient.set(
      'total_websocket_connections',
      allConnectionsSum.toString(),
    );

    await redisClient.del('websocket_connections');

    serverKeys.map(
      async (item, index) =>
        await redisClient.hSet('websocket_connections', {
          [item]: allConnections[index],
        }),
    );
  }
})();
